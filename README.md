# python tools
This is a collection of python functions covering different use-cases:

 * general python tools (mostly in `generic.py` and `caching.py`);
 * pre-processing (`ffmpeg.py`);
 * feature extraction (`features.py` and `audio/*.py`);
 * nominal/ordinal/interval metrics (`ml/metrics.py`); and
 * machine learning related functions (from creating stratified splits, implemented models, ..., to grid-search; `ml/*`).


## Installation
Simply add this repository as a dependency to your project using [poetry](https://python-poetry.org/)
```sh
poetry add git+https://bitbucket.org/twoertwein/python-tools.git
```
and install the following non-python dependencies: ffmpeg, opensmile, OpenFace, and MATLAB. Alternatively, [this](https://bitbucket.org/twoertwein/build_scripts/src/master/image.dockerfile) image contains most of the required non-python dependencies.


## Feature Extraction
A simple example how to extract [OpenFace](https://github.com/TadasBaltrusaitis/OpenFace), [COVAREP](https://github.com/covarep/covarep), [Montreal-Forced-Aligner](https://github.com/MontrealCorpusTools/Montreal-Forced-Aligner), and several [openSMILE](https://www.audeering.com/opensmile/) features:
```python
from pathlib import Path
from python_tools import features

# your input
video_path = Path("test.mkv")
audio_path = Path("test.wav")
transcript_audio_folder = Path(  # needs to contain audio and EAF/TextGrid files
    "transcripts"
)

# what you want to extract and where you want it to be saved (it has to be a hdf file)
# the created hdf files can be opened with pd.read_hdf(filename, "df")
caches = {
    "openface": Path("openface.hdf"),
    "covarep": Path("covarep.hdf"),
    "opensmile_eGeMAPSv02": Path("eGeMAPSv02.hdf"),
    "opensmile_vad_opensource": Path("vad.hdf"),
    "opensmile_prosodyAcf": Path("prosody.hdf"),
    "volume": Path("volume.hdf"),
    "mfa": Path("mfa_output_folder"),
}

# if you have some non-default arguments (e.g., parameters for OpenFace, gender for COVAREP, ...)
kwargs = {"covarep": {"gender": 1}, "mfa": {"folder": transcript_audio_folder}}

# get all the good stuff (if you call it a second time it will read the cached files)
results = features.extract_features(
    video=video_path, audio=audio_path, caches=caches, kwargs=kwargs
)

# results is a dictionary with feature names and Pandas Dataframes as values

# if you want to re-sample them to have the same sample frequency (and an overlapping time interval)
results = features.synchronize_sequences(results)

# results is now one giant Pandas Dataframe
```

### Speaker Diarization
Some 'features' build on top of other features and therefore require (slightly) more complex arguments. Let's assume we have two people and each of them has their own head-mounted microphone but it is possible to overhear the other person:
```python
from python_tools import features

# synchronized audio files of head-mounted microphones
audio_left = Path("person_a.wav")
audio_right = Path("person_b.wav")

# voice activation detection and estimation of time-delay-of-arrival (tdoa) is used for the diarization
# the diarization modules calls extract_features internally to get them
tdoa_cache = Path("tdoa.hdf")
vad_caches = (Path("vad_a.hdf"), Path("vad_b.hdf"))

diarization = features.extract_features(
    audio=audio_left,
    audio2=audio_right,
    caches={"diarization_tdoa": Path("diarization.hdf")},
    kwargs={
        "diarization_tdoa": {
            "tdoa_cache": tdoa_cache,
            "vad_caches": vad_caches,
            "max_shift": 0.01,  # maximal delay between microphones in seconds
            "window_size": 0.2,  # size of the window used to estimate the delay
            "shift": 0.01,  # shift of the window
        }
    },
)
```


## Calculate metrics
Part of `ml.metrics` are three simple functions to calculate many nominal/ordinal/interval metrics:
```python
import numpy as np
from python_tools.ml import metrics

y_true = np.asarray([0, 1, 1, 2, 0, 2])
y_hat = np.asarray([0, 0, 1, 2, 0, 1])
names = ("a", "b", "c")  # class names

scores = np.asarray(  # for AUC/EER
    [
        [0.9, 0.7, 0],
        [0.8, 0.5, 0.6],
        [0.3, 0.9, 0.4],
        [0.3, 0.5, 0.7],
        [0.7, 0.4, 0.6],
        [0.4, 0.8, 0.7],
    ]
)

# nominal metrics
metrics.nominal_metrics(y_true, y_hat, names=names, y_scores=scores)
{
    "confusion___a___a": 2.0,
    "confusion___a___b": 0.0,
    "confusion___a___c": 0.0,
    "confusion___b___a": 1.0,
    "confusion___b___b": 1.0,
    "confusion___b___c": 0.0,
    "confusion___c___a": 0.0,
    "confusion___c___b": 1.0,
    "confusion___c___c": 1.0,
    "macro_precision_a": 0.6666666666666666,
    "macro_precision_b": 0.5,
    "macro_precision_c": 1.0,
    "macro_precision": 0.7222222222222222,
    "macro_recall_a": 1.0,
    "macro_recall_b": 0.5,
    "macro_recall_c": 0.5,
    "macro_recall": 0.6666666666666666,
    "macro_f1_a": 0.8,
    "macro_f1_b": 0.5,
    "macro_f1_c": 0.6666666666666666,
    "macro_f1": 0.6555555555555556,
    "accuracy": 0.6666666666666666,
    "accuracy_balanced": 0.6666666666666666,
    "accuracy_majority": 0.3333333333333333,
    "cohens_kappa": 0.5,
    "krippendorff": 0.5319148936170213,
    "roc_auc_macro_a": 0.875,
    "roc_auc_macro_b": 0.625,
    "roc_auc_macro_c": 1.0,
    "roc_auc_macro": 0.8333333333333334,
    "roc_eer_macro_a": 0.125,
    "roc_eer_macro_b": 0.5,
    "roc_eer_macro_c": 0.0,
    "roc_eer_macro": 0.20833333333333334,
}

# ordinal metrics
metrics.ordinal_metrics(y_true, y_hat)
{
    "krippendorff": 0.763558201058201,
    "cohens_kappa": 0.75,
    "spearman_r": 0.8391463916782735,
    "spearman_p": 0.036729871219315084,
    "icc_2_1": 0.861878453038674,
    "icc_3_1": 0.8387096774193549,
}

# interval metrics
metrics.interval_metrics(y_true, y_hat)
{
    "mse": 0.3333333333333333,
    "mse_mean": 0.6666666666666666,
    "mse_mean_p": 0.15865525393145707,
    "ccc": 0.7499999999999999,
    "pearson_r": 0.8215838362577491,
    "pearson_p": 0.044908790350366615,
    "spearman_r": 0.8391463916782735,
    "spearman_p": 0.036729871219315084,
    "krippendorff": 0.8521505376344086,
}
```
