#!/usr/bin/env python3
# pyright: reportPrivateUsage = false
from functools import partial
from math import isclose
from typing import Any, Optional, cast

import numpy as np
import pandas as pd
import pytest
import torch
from statsmodels.regression.mixed_linear_model import MixedLM

from python_tools.ml.data_loader import DataLoader
from python_tools.ml.default.neural_models import DeepModel, GenericModel, MLPModel
from python_tools.ml.default.transformations import (
    DefaultTransformations,
    set_transform,
)
from python_tools.ml.metrics import interval_metrics
from python_tools.ml.mixed import LinearMixedEffects, NeuralMixedEffects
from python_tools.ml.neural import MLP, LossModule
from python_tools.ml.neural_wrapper import Trainer
from python_tools.typing import DataLoader as DataLoaderType


def get_data() -> DataLoaderType:
    # distance ~ intercept | Subject + age | Subject
    data = pd.read_csv("tests/Orthodont.csv")
    data["Subject"] = pd.Categorical(data["Subject"]).codes

    loader = DataLoader(
        [
            {
                "x": [data["age"].to_numpy().reshape(-1, 1).astype(float)],
                "y": [data["distance"].to_numpy().reshape(-1, 1)],
                "meta_id": [data["Subject"].to_numpy().reshape(-1, 1).astype(int)],
            },
        ],
        properties={
            "y_names": np.asarray(["distance"]),
            "x_names": np.asarray(["age"]),
        },
    )

    # use DefaultTransforms for z-norm to cover more lines
    model_transform = MLPModel(interval=True, device="cpu").get_models()[-1]
    transformer = DefaultTransformations(interval=True)
    loader = set_transform(loader, transformer.define_transform(loader))
    loader.add_transform(model_transform, optimizable=True)
    return loader


def fit(
    model: DeepModel,
    dataloader: DataLoaderType,
    parameter: dict[str, Any],
    abs_tol_optimization: float = 0.05,
    abs_tol_mae: float = 0.05,
) -> Trainer:
    trainer = model._fit(
        dataloader,
        dataloader,
        partial(
            interval_metrics,
            which=("mae", "mse"),
            names=tuple(dataloader.properties["y_names"].tolist()),
        ),
        "",
        **parameter,
    )[0]
    model._predict(trainer, dataloader, "is_ignored")

    assert isclose(
        trainer.metric_matrix["validation_mae"].min(),
        trainer.metric_matrix["validation_mae"].iloc[-1],
        abs_tol=abs_tol_mae,
    )
    assert isclose(
        trainer.metric_matrix["training_optimization"].min(),
        trainer.metric_matrix["training_optimization"].iloc[-1],
        abs_tol=abs_tol_optimization,
    )

    return trainer


def expected_results(reml: bool) -> dict[str, tuple[float, float]]:
    data = pd.read_csv("tests/Orthodont.csv")
    data["age"] = (data["age"] - data["age"].mean()) / data["age"].std()
    data["distance"] = (data["distance"] - data["distance"].mean()) / data[
        "distance"
    ].std()
    data["intercept"] = 1

    model = MixedLM(
        data["distance"],
        data.loc[:, ["age", "intercept"]],
        data["Subject"],
        data.loc[:, ["age", "intercept"]],
    ).fit(reml=reml)

    return {
        "mse": (model.resid.pow(2).mean(), -1.0),
        "age": (model.params["age"], model.bse_fe["age"]),
        "intercept": (model.params["intercept"], model.bse_fe["intercept"]),
        "cov age": (model.cov_re.loc["age", "age"], model.bse_re["age Var"]),
        "cov intercept": (
            model.cov_re.loc["intercept", "intercept"],
            model.bse_re["intercept Var"],
        ),
        "cov age intercept": (
            model.cov_re.loc["age", "intercept"],
            model.bse_re["age x intercept Cov"],
        ),
    }


@pytest.mark.parametrize(("multivariate", "reml"), [(3, False), (1, True)])
def test_linear_mixed_effects(multivariate: int, reml: bool) -> None:
    # same result as with statsmodels
    expected = expected_results(reml)

    dataloader = get_data()
    assert isinstance(dataloader.iterator, list)

    if multivariate > 1:
        tmp = list(dataloader.iterator[0])
        tmp[1] = tmp[1].expand(-1, multivariate)
        dataloader.iterator[0] = tuple(tmp)
        dataloader.properties["y_names"] = np.arange(multivariate).astype(str)

    epochs = 100
    model = MLPModel(interval=True)
    model.parameters.update(
        {
            "epochs": [epochs],
            "lr": [0.1],
            "dropout": [0.0],
            "attenuation": [""],
            "sample_weight": [False],
            "minmax": [False],
            "final_activation": [{"name": "lme", "reml": reml, "iterations": 7}],
            "activation": [{"name": "linear"}],
            "layers": [0],
            "metric": ["epoch"],
            "metric_max": [True],
        },
    )
    parameter = model.get_models()[1][0]
    trainer = fit(model, dataloader, parameter)
    lme = cast(LinearMixedEffects, trainer.model.final_activation)
    mlp = cast(MLP, trainer.model)
    assert trainer.metric_matrix["validation_mse"].iloc[-1] <= expected["mse"][0]

    sigmas = lme.d_sigma * lme.sigma_2.view(-1, 1, 1)

    sigma = sigmas.mean(dim=0)
    assert (sigmas - sigma).abs().max() < 0.02

    assert isclose(
        sigma[0, 0],
        expected["cov age"][0],
        abs_tol=expected["cov age"][1] / 10,
    )
    assert isclose(
        sigma[1, 1],
        expected["cov intercept"][0],
        abs_tol=expected["cov intercept"][1] / 100,
    )
    assert isclose(
        sigma[0, 1],
        expected["cov age intercept"][0],
        abs_tol=expected["cov age intercept"][1] / 100,
    )
    assert isclose(
        mlp.layers[0].weight[0, 0],
        expected["age"][0],
        abs_tol=expected["age"][1] / 50,
    )
    assert isclose(
        mlp.layers[0].bias[0],
        expected["intercept"][0],
        abs_tol=expected["intercept"][1] / 60,
    )

    # test add_new_random_effects
    y_hat = trainer.model(
        dataloader.iterator[0][0],
        dataloader.iterator[0][2].copy(),
        y=dataloader.iterator[0][1],
    )[0]
    y_fixed = trainer.model(
        dataloader.iterator[0][0],
        {"meta_id": -torch.ones(dataloader.iterator[0][0].shape)},
        y=dataloader.iterator[0][1],
    )[0]
    y_y_fixed = dataloader.iterator[0][1] - y_fixed
    fake_ids = -(1 + dataloader.iterator[0][2]["meta_id"])
    index = (fake_ids != fake_ids[-1]).view(-1)
    lme.add_new_random_effects(
        dataloader.iterator[0][0][index],
        y_y_fixed[index],
        fake_ids[index],
    )

    # same (or better) as when used during training
    y_new = trainer.model(
        dataloader.iterator[0][0][index],
        {"meta_id": fake_ids[index]},
        y=dataloader.iterator[0][1][index],
    )[0]
    assert (y_new - dataloader.iterator[0][1][index]).abs().mean() - (
        y_hat[index] - dataloader.iterator[0][1][index]
    ).abs().mean() < 1e-3


def test_nonlinear_mixed_effects() -> None:
    # same result as with statsmodels
    dataloader = get_data()

    epochs = 100
    model = GenericModel(interval=True)
    model.parameters.update(
        {
            "model_fun": [MLP],
            "model_class": [NeuralMixedEffects],
            "epochs": [epochs],
            "lr": [0.1],
            "attenuation": [""],
            "sample_weight": [False],
            "minmax": [False],
            "model_dropout": [0.0],
            "model_final_activation": [{"name": "linear"}],
            "model_activation": [{"name": "linear"}],
            "model_layers": [0],
            "model_layer_sizes": [None],
            "random_effects": [("layers.0.bias", "layers.0.weight")],
            "metric": ["epoch"],
            "metric_max": [True],
        },
    )
    del model.parameters["final_activation"]
    model.forward_names = (
        *model.forward_names,
        "model_dropout",
        "model_final_activation",
        "model_activation",
        "model_layers",
        "model_layer_sizes",
        "model_fun",
        "random_effects",
        "sample_every_nth_epoch",
        "clusters",
        "cluster_count",
    )
    parameter = model.get_models()[1][0]
    trainer = fit(model, dataloader, parameter, abs_tol_optimization=1.0)
    expected = expected_results(False)
    assert trainer.metric_matrix.loc[epochs - 1, "training_mse"] <= expected["mse"][0]

    # get random/fixed
    nme = cast(NeuralMixedEffects, trainer.model)
    parameters = torch.cat(
        [
            (nme.fixed["layers-0-weight"] + nme.random["layers-0-weight"]).view(-1, 1),
            nme.fixed["layers-0-bias"] + nme.random["layers-0-bias"],
        ],
        dim=-1,
    )
    fixed = parameters.mean(dim=0, keepdim=True)
    random = parameters - fixed

    sigma = torch.cov(random.t())
    assert isclose(sigma[0, 0], expected["cov age"][0], abs_tol=expected["cov age"][1])
    assert isclose(
        sigma[1, 1],
        expected["cov intercept"][0],
        abs_tol=expected["cov intercept"][1] / 8,
    )
    assert isclose(
        sigma[0, 1],
        expected["cov age intercept"][0],
        abs_tol=expected["cov age intercept"][1] / 30,
    )
    assert isclose(fixed[0, 0], expected["age"][0], abs_tol=expected["age"][1] / 100)
    assert isclose(
        fixed[0, 1],
        expected["intercept"][0],
        abs_tol=expected["intercept"][1] / 100,
    )


def get_nonlinear_data() -> DataLoaderType:
    data = pd.read_csv("tests/pd2saemix.csv", index_col=0)
    data["subject"] = pd.Categorical(data["subject"]).codes

    loader = DataLoader(
        [
            {
                "x": [data["dose"].to_numpy().reshape(-1, 1).astype(float)],
                "y": [data["response"].to_numpy().reshape(-1, 1)],
                "meta_id": [data["subject"].to_numpy().reshape(-1, 1).astype(int)],
            },
        ],
        properties={
            "y_names": np.asarray(["response"]),
            "x_names": np.asarray(["dose"]),
        },
    )
    # use DefaultTransforms for z-norm to cover more lines
    model_transform = MLPModel(interval=True, device="cpu").get_models()[-1]
    transformer = DefaultTransformations(interval=True).define_transform(loader)
    loader = set_transform(loader, transformer)
    loader.add_transform(model_transform, optimizable=True)
    return loader


# skipped on the CI
def skipped_test_nonlinear_mixed_effects2() -> None:
    # same result as with saemix
    """library(saemix).

    data(PD2.saemix)
    PD2.saemix$dose  <- (
        PD2.saemix$dose - mean(PD2.saemix$dose)
    ) / sd(PD2.saemix$dose)
    PD2.saemix$response  <- (
        PD2.saemix$response - mean(PD2.saemix$response)
    ) / sd(PD2.saemix$response)

    saemix.data2<-saemixData(
        name.data=PD2.saemix,
        header=TRUE,
        name.group=c("subject"),
        name.predictors=c("dose"),
        name.response=c("response"),
        units=list(x="mg",y="-")
    )

    modelemax<-function(psi,id,xidep) {
        dose<-xidep[,1]
        e0<-psi[id,1]
        emax<-psi[id,2]
        e50<-psi[id,3]
        f<-e0+emax*dose/(e50+dose)
        return(f)
    }

    saemix.model<-saemixModel(
        model=modelemax,
        psi0=matrix(c(1,1,1),ncol=3,byrow=TRUE,
        dimnames=list(NULL,c("E0","Emax","EC50"))),
        transform.par=c(0,0,0),
        fixed.estim=c(1,1,1)
    )

    saemix(saemix.model, saemix.data2)
    """
    dataloader = get_nonlinear_data()

    epochs = 275
    model = GenericModel(interval=True)
    model.parameters.update(
        {
            "model_fun": [PD2saemix],
            "model_class": [NeuralMixedEffects],
            "epochs": [epochs],
            "lr": [0.05],
            "random_effects": [("e_zero", "e_max", "e_fifty")],
            "independent": [("e_zero", "e_max", "e_fifty")],
            "metric": ["epoch"],
            "metric_max": [True],
            "simulated_annealing_alpha": [0.99],
        },
    )
    del model.parameters["final_activation"]
    model.forward_names = (
        *model.forward_names,
        "model_fun",
        "random_effects",
        "clusters",
        "cluster_count",
        "independent",
        "sample_every_nth_epoch",
        "simulated_annealing_alpha",
    )
    parameter = model.get_models()[1][0]
    trainer = fit(
        model,
        dataloader,
        parameter,
        abs_tol_optimization=2,  # not relevant
        abs_tol_mae=0.2,
    )
    assert trainer.metric_matrix.loc[epochs - 1, "training_mse"] <= 0.098

    # get random/fixed
    nme = cast(NeuralMixedEffects, trainer.model)
    parameters = torch.cat(
        [
            (nme.fixed["e_zero"] + nme.random["e_zero"]).view(-1, 1),
            (nme.fixed["e_max"] + nme.random["e_max"]).view(-1, 1),
            (nme.fixed["e_fifty"] + nme.random["e_fifty"]).view(-1, 1),
        ],
        dim=-1,
    )
    fixed = parameters.mean(dim=0, keepdim=True)
    random = parameters - fixed

    variance = random.var(dim=0)
    assert isclose(variance[0], 0.6277, abs_tol=0.0894)
    assert isclose(variance[1], 0.1108, abs_tol=0.0179)
    assert isclose(variance[2], 0.0054, abs_tol=0.0031)


class PD2saemix(LossModule):
    """Model for PD2.saemix."""

    def __init__(self, **kwargs: Any) -> None:
        """Model has three mixed effects."""
        super().__init__()

        self.e_zero = torch.nn.Parameter(torch.ones(1))
        self.e_max = torch.nn.Parameter(torch.ones(1))
        self.e_fifty = torch.nn.Parameter(torch.ones(1))

    def forward(
        self,
        x: torch.Tensor,
        meta: dict[str, torch.Tensor],
        y: Optional[torch.Tensor] = None,
        dataset: str = "",
    ) -> tuple[torch.Tensor, dict[str, torch.Tensor]]:
        """Predict drug response given the doseage."""
        return self.e_zero + self.e_max * x / (self.e_fifty + x), meta
