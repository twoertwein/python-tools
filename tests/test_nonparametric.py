#!/usr/bin/env python3
from math import isclose
from typing import cast

import numpy as np
import pandas as pd
import pytest

from python_tools.ml.data_loader import DataLoader
from python_tools.ml.default.neural_models import GenericModel, MLPModel
from python_tools.ml.default.transformations import (
    DefaultTransformations,
    set_transform,
)
from python_tools.ml.neural import MLP
from python_tools.ml.nonparametric import (
    DWACActivation,
    GPVFEActivation,
    SelectiveTransferMachine,
)
from python_tools.typing import DataLoader as DataLoaderType
from tests.test_mixed_effects import fit, get_data


@pytest.mark.parametrize(
    ("regression", "similarity"),
    [(False, "rbf_similarity"), (True, "frechet_similarity")],
)
def test_dwac(regression: bool, similarity: str) -> None:
    dataloader = get_data()
    assert isinstance(dataloader.iterator, list)
    if not regression:
        tmp = list(dataloader.iterator[0])
        tmp[1] = (tmp[1] < -0.3).to(dtype=int) + (tmp[1] < 0.3).to(dtype=int)
        dataloader.iterator[0] = tuple(tmp)
        dataloader.properties["y_names"] = np.array(["0", "1", "2"])

    epochs = 100
    model = MLPModel(interval=regression, nominal=not regression)
    model.parameters.update(
        {
            "epochs": [epochs],
            "lr": [0.01],
            "dropout": [0.0],
            "attenuation": [""],
            "sample_weight": [False],
            "minmax": [False],
            "final_activation": [
                {
                    "name": "dwac",
                    "embedding_size": 1,
                    "exclude_same_id": regression,
                    "similarity": similarity,
                    "cache_keys": ("unique_ids_t", "means_t", "covs_t", "id_t_index")
                    if regression
                    else ("x_t_norm",),
                    "clustered_similarity": regression,
                },
            ],
            "activation": [{"name": "linear"}],
            "layers": [0],
            "metric": ["epoch"],
            "metric_max": [True],
        },
    )
    parameter = model.get_models()[1][0]
    trainer = fit(model, dataloader, parameter)
    dwac = cast(DWACActivation, trainer.model.final_activation)
    assert dwac.y.shape[0] != 0
    assert isclose(
        trainer.metric_matrix["training_optimization"].min(),
        1.3975578546524048 if regression else 0.8903449177742004,
        rel_tol=1e-4,
    )


def test_gpvfe() -> None:
    dataloader = get_snelson_data()

    epochs = 150
    model = MLPModel(interval=True)
    model.parameters.update(
        {
            "epochs": [epochs],
            "lr": [0.03],
            "dropout": [0.0],
            "attenuation": [""],
            "sample_weight": [False],
            "minmax": [False],
            "final_activation": [
                {
                    "name": "gpvfe",
                    "embedding_size": 1,
                    "inducing_points": 15,
                    "similarity": "rbf_similarity",
                },
            ],
            "activation": [{"name": "linear"}],
            "layers": [0],
            "metric": ["epoch"],
            "metric_max": [True],
        },
    )
    parameter = model.get_models()[1][0]
    trainer = fit(model, dataloader, parameter, abs_tol_optimization=2.2)
    gpvfe = cast(GPVFEActivation, trainer.model.final_activation)
    assert gpvfe.k_m_n_y.shape[0] != 0
    assert isclose(
        trainer.metric_matrix["training_optimization"].min(),
        89.888336,
        rel_tol=1e-2,
    )
    assert isclose(gpvfe.length_scale[0], 1.3694, rel_tol=1e-2)
    assert isclose(gpvfe.observation_noise[0], -2.1876, rel_tol=1e-2)


def get_snelson_data() -> DataLoaderType:
    data = pd.read_csv("tests/snelson.csv")
    loader: DataLoaderType = DataLoader(
        [
            {
                "x": [data["x"].to_numpy().reshape(-1, 1).astype(float)],
                "y": [data["y"].to_numpy().reshape(-1, 1)],
                "meta_id": [data.index.to_numpy().reshape(-1, 1)],
            },
        ],
        properties={
            "y_names": np.asarray(["x"]),
            "x_names": np.asarray(["y"]),
        },
    )

    # use DefaultTransforms for z-norm to cover more lines
    model_transform = MLPModel(interval=True, device="cpu").get_models()[-1]
    transformer = DefaultTransformations(interval=True)
    loader = set_transform(loader, transformer.define_transform(loader))
    loader.add_transform(model_transform, optimizable=True)
    return loader


@pytest.mark.parametrize("vfkmm", [False, True])
def test_stm(vfkmm: bool) -> None:
    dataloader = get_quadratic_data()
    assert isinstance(dataloader.iterator, list)

    # optimize for subset
    index = dataloader.iterator[0][2]["meta_id"].view(-1) < 10
    data = (
        dataloader.iterator[0][0][index],
        {"meta_id": dataloader.iterator[0][2]["meta_id"][index]},
    )

    epochs = 100
    model = GenericModel(interval=True)
    model.parameters.update(
        {
            # general
            "epochs": [epochs],
            "lr": [0.01],
            "attenuation": [""],
            "sample_weight": [False],
            "minmax": [False],
            "model_class": [SelectiveTransferMachine],
            "metric": ["epoch"],
            "metric_max": [True],
            # linear model
            "model_fun": [MLP],
            "model_final_activation": [{"name": "linear"}],
            "model_activation": [{"name": "linear"}],
            "model_layers": [0],
            # STM
            "similarity": ["rbf_similarity"],
            "log_scale": [0.0],
            "loss_lambda": [0.0],
            "embedding_size": [1],
            "data": [data],
        },
    )
    if vfkmm:
        model.parameters["sample_size"] = [15]
        model.parameters["tolerance"] = [0.01]

    model.forward_names = (
        *model.forward_names,
        "model_fun",
        "model_final_activation",
        "model_activation",
        "model_layers",
        "similarity",
        "log_scale",
        "loss_lambda",
        "embedding_size",
        "data",
        "sample_size",
        "tolerance",
    )
    model.static_kwargs = (*model.static_kwargs, "final_activation")
    parameter = model.get_models()[1][0]
    trainer = fit(model, dataloader, parameter, abs_tol_optimization=0.5)
    subset = (
        (trainer.model(data[0], data[1])[0] - dataloader.iterator[0][1][index])
        .abs()
        .mean()
    )
    assert isclose(subset.item(), 0.6677424311637878, rel_tol=5e-2)


def get_quadratic_data() -> DataLoaderType:
    x_b = np.sort(np.random.default_rng(1).random(20) * 2 - 1)
    y_true = x_b**2
    x_b = (x_b - x_b.mean()) / x_b.std()
    y_true = (y_true - y_true.mean()) / y_true.std()
    data = pd.DataFrame({"x": x_b, "y": y_true})
    loader: DataLoaderType = DataLoader(
        [
            {
                "x": [data["x"].to_numpy().reshape(-1, 1).astype(float)],
                "y": [data["y"].to_numpy().reshape(-1, 1)],
                "meta_id": [data.index.to_numpy().reshape(-1, 1)],
            },
        ],
        properties={
            "y_names": np.asarray(["x"]),
            "x_names": np.asarray(["y"]),
        },
    )

    # use DefaultTransforms for z-norm to cover more lines
    model_transform = MLPModel(interval=True, device="cpu").get_models()[-1]
    transformer = DefaultTransformations(interval=True)
    loader = set_transform(loader, transformer.define_transform(loader))
    loader.add_transform(model_transform, optimizable=True)
    return loader
