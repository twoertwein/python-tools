#!/usr/bin/env python3
import shutil
from functools import partial
from pathlib import Path
from typing import Any

import numpy as np
import pandas as pd
import pytest

from python_tools.ml.data_loader import DataLoader
from python_tools.ml.default.models import SVMModel
from python_tools.ml.default.transformations import (
    DefaultTransformations,
    revert_transform,
    set_transform,
)
from python_tools.ml.evaluator import evaluator
from python_tools.ml.metrics import interval_metrics


@pytest.mark.parametrize("workers", [1, 4])
def test_svr(workers: int) -> None:
    # create dataloader
    size = 100
    partitions = {
        fold: {
            name: DataLoader(
                [
                    {
                        "x": [np.arange(size).reshape(-1, 1)],
                        "y": [np.arange(size).reshape(-1, 1)],
                        "meta_id": [np.arange(size, dtype=int).reshape(-1, 1)],
                    },
                ],
                properties={
                    "y_names": np.array(["x1"]),
                    "x_names": np.array(["test"]),
                },
            )
            for name in ("training", "validation", "test")
        }
        for fold in range(5)
    }

    # create model
    model = SVMModel(linear=True, interval=True)
    models, parameters = model.get_models()[:2]
    models = models[:4]
    parameters = parameters[:4]

    # create transformation
    transform = DefaultTransformations(interval=True)
    transforms: tuple[dict[str, Any], ...] = tuple([{}] * len(partitions))

    folder = Path(f"evaluator_{workers}")
    overview = _evaluator(
        models=models,
        parameters=parameters,
        partitions=partitions,
        folder=folder,
        metric_fun=partial(interval_metrics, which=("mae",)),
        metric="mae",
        metric_max=False,
        learn_transform=transform.define_transform,
        apply_transform=set_transform,
        revert_transform=revert_transform,
        transform_parameter=transforms,
        workers=workers,
    )
    assert overview["test_mae"].min() < 8.0


def _evaluator(**kwargs: Any) -> pd.DataFrame:
    try:
        overview = evaluator(**kwargs)
        overview = evaluator(**kwargs)
    finally:
        shutil.rmtree(kwargs["folder"])
    return overview
