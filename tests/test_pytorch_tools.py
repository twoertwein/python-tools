#!/usr/bin/env python3
import numpy as np
import torch

from python_tools.ml.neural import sqrtm, trace_sqrtm, unique_with_index
from python_tools.ml.pytorch_tools import list_to_packedsequence, packedsequence_to_list


def test_unique_with_index() -> None:
    data = (np.random.default_rng(1).random(100) * 50).round()
    expected_unique, epected_indices = np.unique(data, return_index=True)
    unique, indices = unique_with_index(torch.from_numpy(data))
    assert (unique.numpy() == expected_unique).all()
    assert (indices.numpy() == epected_indices).all()


def test_packed_sequence_format() -> None:
    """Test that the internal representation of PackedSequence didn't change."""
    data = torch.zeros(10, 3)
    indices = ([0] * 7) + ([1] * 3)
    data[:7, 1] = -1
    data[7:, 1] = 1
    data[:7, 2] = torch.arange(7)
    data[7:, 2] = torch.arange(3)
    packed = list_to_packedsequence([data, indices])

    assert (packed.data[:, 0] == 0).all()
    assert (
        packed.data[:, 1] == torch.Tensor([-1, 1, -1, 1, -1, 1, -1, -1, -1, -1])
    ).all()
    assert (packed.data[:, 2] == torch.Tensor([0, 0, 1, 1, 2, 2, 3, 4, 5, 6])).all()


def test_pack_unpack() -> None:
    lengths = (100, 400, 300, 200)
    data = torch.rand(sum(lengths), 10)
    indices: list[int] = sum(
        ([index] * length for index, length in enumerate(lengths)),
        start=[],
    )
    packed = list_to_packedsequence([data, indices])
    unpacked = packedsequence_to_list(packed)
    for i, (length, end) in enumerate(zip(lengths, np.cumsum(lengths), strict=True)):
        assert (unpacked[i] == data[end - length : end]).all()


def test_sqrtm() -> None:
    x_ii = torch.rand(10, 10)
    x_ii = x_ii.mm(x_ii.t()) + 10 * torch.eye(10)
    # batched
    root = sqrtm(x_ii.unsqueeze(0)).squeeze(0)
    assert torch.allclose(x_ii, root.mm(root))
    # not batched
    root = sqrtm(x_ii)
    assert torch.allclose(x_ii, root.mm(root))


def test_trace_sqrtm() -> None:
    x_ii = torch.rand(10, 10)
    x_ii = x_ii.mm(x_ii.t()) + 10 * torch.eye(10)
    assert torch.allclose(trace_sqrtm(x_ii), torch.trace(sqrtm(x_ii)))
