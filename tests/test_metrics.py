#!/usr/bin/env python3
# pyright: reportGeneralTypeIssues = false
from math import isclose

import numpy as np
import pytest
import sklearn.metrics
from nltk.metrics import binary_distance, interval_distance
from nltk.metrics.agreement import AnnotationTask
from scipy.special import softmax
from scipy.stats import pearsonr, spearmanr

from python_tools.ml import metrics
from python_tools.ml.metrics import (
    concordance_correlation_coefficient,
    intraclass_correlation_coefficient,
)


@pytest.mark.parametrize(
    "metric",
    ["mse", "mae", "pearson", "spearman", "krippendorff"],
)
@pytest.mark.parametrize("clusters", [False, True])
def test_interval(metric: str, clusters: bool) -> None:
    rng = np.random.default_rng(1)
    number_of_samples = 1_000
    y_true = rng.random(number_of_samples)
    y_hat = rng.random(number_of_samples)
    ids = None
    if clusters:
        ids = np.ones_like(y_true)

    for key, value in metrics.interval_metrics(
        y_true,
        y_hat,
        which=(metric,),
        ids=ids,
        clustering=clusters,
    ).items():
        assert not isinstance(value, np.number)
        match key:
            case "mse":
                expected = sklearn.metrics.mean_squared_error(y_true, y_hat)
            case "mae":
                expected = sklearn.metrics.mean_absolute_error(y_true, y_hat)
            case "pearson_r":
                expected = pearsonr(y_true, y_hat)[0]
            case "spearman_r":
                expected = spearmanr(y_true, y_hat)[0]
            case "krippendorff":
                expected = AnnotationTask(
                    data=[
                        (label, irow, value)
                        for label, values in (("true", y_true), ("predicted", y_hat))
                        for irow, value in enumerate(values)
                    ],
                    distance=interval_distance,
                ).alpha()
            case "cluster_average":
                assert (value > 0.5) == clusters
                continue
            case _:
                assert key.endswith(("_p", "_proportional")) or key in (
                    "mse_mean",
                    "mae_median",
                )
                continue

        assert isclose(value, expected), key


@pytest.mark.parametrize(
    "metric",
    ["macro", "accuracy", "cohens_kappa", "roc", "brier", "krippendorff"],
)
@pytest.mark.parametrize("classes", [2, 5])
def test_nominal(metric: str, classes: int) -> None:
    rng = np.random.default_rng(1)
    number_of_samples = 1_000

    y_scores = rng.random(size=(number_of_samples, classes))
    y_scores = (y_scores == y_scores.max(axis=1, keepdims=True)).astype(float)
    y_true = np.argmax(y_scores, axis=1)
    y_hat_scores = rng.random(size=(number_of_samples, classes))
    y_hat = np.argmax(y_hat_scores, axis=1)

    names = tuple(str(iclass) for iclass in range(classes))

    for key, value in metrics.nominal_metrics(
        y_true,
        y_hat,
        which=(metric,),
        names=names,
        y_scores=y_hat_scores,
    ).items():
        assert not isinstance(value, np.number)
        match key:
            case "brier_score" | "roc_eer_macro" if classes != 2:
                continue
            case "accuracy":
                expected = sklearn.metrics.accuracy_score(y_true, y_hat)
            case "accuracy_balanced":
                expected = sklearn.metrics.balanced_accuracy_score(y_true, y_hat)
            case "cohens_kappa":
                expected = sklearn.metrics.cohen_kappa_score(y_true, y_hat)
            case "macro_f1":
                expected = sklearn.metrics.f1_score(y_true, y_hat, average="macro")
            case "macro_recall":
                expected = sklearn.metrics.recall_score(y_true, y_hat, average="macro")
            case "macro_precision":
                expected = sklearn.metrics.precision_score(
                    y_true,
                    y_hat,
                    average="macro",
                )
            case "roc_auc_macro":
                expected = sklearn.metrics.roc_auc_score(
                    y_scores,
                    softmax(y_hat_scores, axis=1),
                    average="macro",
                )
            case "roc_eer_macro":
                # approximation stackoverflow.com/a/46026962
                false_pos_rate, true_pos_rate = sklearn.metrics.roc_curve(
                    y_true,
                    softmax(y_hat_scores, axis=1)[:, 1],
                )[:2]
                false_neg_rate = 1 - true_pos_rate
                argmin = np.nanargmin(np.absolute(false_neg_rate - false_pos_rate))
                expected = (false_pos_rate[argmin] + false_neg_rate[argmin]) / 2
                assert isclose(value, expected, abs_tol=5e-3), key
                continue
            case "brier_score":
                expected = sklearn.metrics.brier_score_loss(
                    y_true,
                    softmax(y_hat_scores, axis=1)[:, 1],
                )
            case "krippendorff":
                expected = AnnotationTask(
                    data=[
                        (label, irow, value)
                        for label, values in (("true", y_true), ("predicted", y_hat))
                        for irow, value in enumerate(values)
                    ],
                    distance=binary_distance,
                ).alpha()
            case _:
                assert key[-1] in names or key in (
                    "cluster_average",
                    "accuracy_majority",
                )
                continue
        assert isclose(value, expected), key


@pytest.mark.parametrize("dimensions", [1, 2])
def test_ccc(dimensions: int) -> None:
    # same result as with epiR.ccc
    y_true = np.arange(5)[:, None] + 1
    y_hat = np.array([1, 4, 9, 16, 25])[:, None]
    expected = [0.1704545]
    if dimensions == 2:
        y_hat = np.concatenate([y_hat, np.array([0, 2, 4, 4, 6])[:, None]], axis=1)
        y_true = np.concatenate([y_true, y_true], axis=1)
        expected = [expected[0], 0.9032258]
    actual = concordance_correlation_coefficient(y_true, y_hat)["ccc"]

    assert len(actual) == dimensions
    for i, expected_i in enumerate(expected):
        assert not isinstance(actual[i], np.number)
        assert isclose(actual[i], expected_i, rel_tol=1e-5)


def test_icc() -> None:
    # same result as with psy's icc
    # library('psy')
    # icc(matrix(c(9, 6, 8, 7, 10, 6, 2, 1, 4, 1, 5, 2), ncol=2))
    y_true = np.array([9, 6, 8, 7, 10, 6]).reshape(-1, 1)
    y_true = np.concatenate([y_true] * 3, axis=1)
    y_hat = np.array([2, 1, 4, 1, 5, 2]).reshape(-1, 1)
    y_hat = np.concatenate([y_hat] * 3, axis=1)
    icc = intraclass_correlation_coefficient(y_true, y_hat)
    assert not isinstance(icc["icc_c_1"][0], np.number)
    assert isclose(0.7453416, icc["icc_c_1"][0], rel_tol=1e-6)
    assert isclose(icc["icc_c_1"][0], icc["icc_c_1"][1])
    assert isclose(icc["icc_c_1"][0], icc["icc_c_1"][2])
    assert not isinstance(icc["icc_a_1"][0], np.number)
    assert isclose(0.1256545, icc["icc_a_1"][0], rel_tol=1e-6)
    assert isclose(icc["icc_a_1"][0], icc["icc_a_1"][1])
    assert isclose(icc["icc_a_1"][0], icc["icc_a_1"][2])
